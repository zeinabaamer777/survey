$(document).ready(function() {
    $("#retrieve-resources").click(function() {
      var displayResources = $("#display-resources");
  
      displayResources.text("Loading data from JSON source...");
      $.ajax({
        type: "GET",
        dataType: "json",
        url: "https://api.myjson.com/bins/owmcc", 
        success: function(result) {
          console.log(result);
          var output =
            `<table class="display table table-striped"><thead><tr><th>Name</th><th>Provider</th><th>URL</th></thead><tbody>`;
            for (var i in result) {
                output += `<tr><td> ${result[i].name} </td><td> ${result[i].provider} </td><td> ${result[i].url}</td>`;
            }
        //   output += "</tr></tbody></table>";
  
          displayResources.html(output);
        }
      });
    });
  });
  